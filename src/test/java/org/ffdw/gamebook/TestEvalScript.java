package org.ffdw.gamebook;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class TestEvalScript {

	public static void main(String[] args) {
		// create a script engine manager
		ScriptEngineManager factory = new ScriptEngineManager();
		// create a JavaScript engine
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		// evaluate JavaScript code from String
		try {
			Object result = engine.eval("a=3+4");
			result = engine.eval("a=a+4");
			System.out.println(result);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		String name = "Falco";
		int stamina = 10;
		int abilita = 5;
		int mana = 8;
		NotPlayer np = new NotPlayer(name, stamina, abilita, mana);
		engine.put("np", np);
		try {
			Object result = engine.eval("np.mana>4 && np.mana<11");
			System.out.println(result);
			result = engine.eval("np.setMana(4)");
			result = engine.eval("np.mana=7");
			result = engine.eval("np.getMana()");
			System.out.println("mana:" + result);
			result = engine.eval("np.putSpecial('gold',4)");
			result = engine.eval("np.getSpecial('gold')");
			System.out.println("gold:" + result);
			result = engine.eval("np.att('gold',8.5)");
			result = engine.eval("np.att('gold')");
			System.out.println("gold att:" + result);
			result = engine.eval("np.inc('gold')");
			System.out.println("gold att:" + result);
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
