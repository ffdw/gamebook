package org.ffdw.gamebook;

public class TestGetSetString {

	public static void main(String[] args) throws Exception {
		testOggetto();
		testPlayer();
	}

	private static void testOggetto() throws Exception {
		Oggetto o = new Oggetto();
		String result = o.getString();
		String expected = "null,null,0,0,0,0,0,1,1";
		assert result.equals(expected) : expected + " != result " + result;

		o.setAttacco(12);
		result = o.getString();
		expected = "null,null,0,0,0,12,0,1,1";
		assert result.equals(expected) : expected + " != result " + result;

		o = new Oggetto("oname", 4, 5, 6, 7);
		result = o.getString();
		expected = "oname,null,5,6,7,0,0,4,1";
		assert result.equals(expected) : expected + " != result " + result;
	}

	private static void testPlayer() throws Exception {
		Player p = new Player();
		String result = p.getString();
		String expected = "noname,,0,0,0,0,0,0,,0,0,0,";
		assert result.equals(expected) : expected + " != result " + result;

		p = new Player();
		p.setNome("nome");
		p.setGilda("gilda");
		p.setAbilita(12);
		p.setStamina(14);
		p.setFato(2);
		p.setInUso(new Oggetto());
		result = p.getString();
		expected = "nome,gilda,12,14,0,0,0,0,,0,0,0,";
		assert result.equals(expected) : expected + " != result " + result;
	}

}
