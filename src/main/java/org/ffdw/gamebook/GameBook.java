package org.ffdw.gamebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameBook {
	protected Book book;
	protected Player p;
	protected NotPlayer np;
	protected List<NotPlayer> registroIncontri;
	protected Battle battle;
	protected List<Bookmark> bookmarks;
	protected String currentPageKey;
	protected IEval engine = new EvalRhino();

	public GameBook() {
		p = new Player().randomize();
		registroIncontri = new ArrayList<NotPlayer>();
		bookmarks = new ArrayList<Bookmark>();
		initEngine();
	}

	public void initEngine() {
		try {
			engine.eval("function dado(v) {return Math.floor((Math.random() * v) + 1); }");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Page getPage(String pageKey) {
		this.currentPageKey = pageKey;
		return book.getPage(pageKey);
	}

	public Player getP() {
		return p;
	}

	public void setP(Player p) {
		this.p = p;
	}

	public NotPlayer getNp() {
		return np;
	}

	public void setNp(NotPlayer np) {
		this.np = np;
	}

	public Battle getBattle(Page page) {
		if (page == null)
			return null;
		if (np == null) {
			np = page.np.duplicate();
		}
		registroIncontri.add(np);
		battle = new Battle();
		if (battle == null && np != null) {
			battle = new Battle();
			battle.setNotPlayer(np);
			// page.battle = battle;//TODO remove
		}
		return battle;
	}

	public static void main(String[] args) {
		BookImporter bookImporter = new BookImporter();
		String bookName = "specchio.txt";
		Book book = bookImporter.read(bookName);
		System.out.println("BOOK TITLE:" + book.getTitle() + " ("
				+ book.getNrPages() + " pages)");
		Page introPage = book.getPage("INTRO");
		System.out.println("BOOK INTRO:" + introPage);

		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);

		// Creazione personaggio;
		Player p = CreazionePlayer.build(in, book);
		System.out.println("PLAYER:" + p);

		// Inizio avventura;

		Page prologoPage = book.getPage("PROLOGO");
		System.out.println("PROLOGO:" + prologoPage);
		System.out.println("DIRECTIONS:");
		for (String direction : prologoPage.directions) {
			System.out.println(direction);
		}

		boolean exit = false;
		while (!exit) {
			try {
				System.out.println("BOOK PAGE ?");
				String key = in.readLine();
				if ("exit".equals(key)) {
					exit = true;
				} else if ("status".equals(key)) {
					System.out.println(p);
				} else {
					Page page = book.getPage(key);
					if (page != null) {
						playPage(p, in, page);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Return an array (text,image,dirpage[i],dirdesc[i])
	 * 
	 * @param pageName
	 * @return
	 */
	public List<String> playPage(String pageName) {
		Page page = getPage(pageName);
		String texto = page.toString();
		texto = texto.replace('"', ' ');
		texto = texto.replace('\n', ' ');
		List<String> actions = page.getActions();
		if (actions != null) {
			for (String action : actions) {
				if (action.startsWith("{S:")) {
					String[] specialTokens = action.split("{|}|:|=");
					if (specialTokens.length == 2) {
						getP().putSpecial(specialTokens[1], specialTokens[1]);
					} else if (specialTokens.length == 3) {
						getP().putSpecial(specialTokens[1], specialTokens[2]);
					}

				} else if (action.startsWith("{mana")) {
					getP().addMana(new Integer(action.substring(5, 8)));
				} else if (action.startsWith("{stamina")) {
					String substring = action.substring(8, 11);
					getP().addStamina(new Integer(substring));
				} else if (action.startsWith("{abilita")) {
					String substring = action.substring(8, 11);
					getP().addAbilita(new Integer(substring));
				} else if (action.startsWith("{experience")) {
					String substring = action.substring(11, 14);
					getP().addExperience(new Integer(substring));
				} else {
					// else do javascript eval
					// costruisce il contesto
					Map<String, Object> arguments = new HashMap<String, Object>();
					arguments.put("p", getP());
					arguments.put("np", getNp());
					Object result;
					try {
						result = engine.eval(action, arguments);
					} catch (Exception e) {
						result = e.getMessage();
					}
					texto += " =(" + result + ")";
				}

			}
		}
		List<String> arrayObj = new ArrayList<String>();
		arrayObj.add(texto);
		arrayObj.add(page.getImage());
		// TODO form page
		// luck page (fight page)
		if (page.getConditions() != null) {
			for (String condition : page.getConditions()) {
				String alternative = page.getAlternative();
				if (condition.startsWith("[abilita")) {
					return provaAbilita(page, arrayObj, condition, alternative);
				} else if (condition.startsWith("[whell")) {
					return whell(page, arrayObj, condition);
				} else if (condition.startsWith("[S:")) {
					return provaSpeciali(page, arrayObj, condition);
				} else if (condition.startsWith("[C:")) {
					return provaFight(page, arrayObj, condition, alternative);
				} else {
					conditionalLink(page, condition, alternative);
				}
			}
		}
		// link page

		List<String> directionsPages = page.getDirectionsPages();
		List<String> directionsDescription = page.getDirectionsDescription();
		if (directionsPages != null) {
			for (int i = 0; i < directionsPages.size(); i++) {
				arrayObj.add(directionsPages.get(i));
				arrayObj.add(directionsDescription.get(i));
			}
		}
		// include page
		if (page.getIncludePages() != null) {
			for (String pageToInc : page.getIncludePages()) {

				List<String> includedObj = playPage(pageToInc);
				arrayObj.set(0, arrayObj.get(0) + includedObj.get(0));// text
				arrayObj.set(1, includedObj.get(0));// image
				includedObj.remove(0);
				includedObj.remove(0);
				arrayObj.addAll(includedObj);// directions
			}
		}

		return arrayObj;
	}

	// si assume sintassi ]--->
	private String getConditionDirPageNr(String condition) {
		int endCondition = condition.indexOf(']');
		int virgola = condition.indexOf(',', endCondition);
		String pageNr;
		if (virgola > 0) {
			pageNr = condition.substring(endCondition + 5, virgola);
		} else {
			pageNr = condition.substring(endCondition + 5);
		}
		return pageNr;
	}

	// si assume sintassi ]--->
	private String getConditionDirDescription(String condition) {
		int endCondition = condition.indexOf(']');
		int virgola = condition.indexOf(',', endCondition);
		String pageDesc;
		if (virgola > 0) {
			pageDesc = condition.substring(virgola + 1);
		} else {
			pageDesc = condition.substring(endCondition + 5);
		}
		return pageDesc;
	}

	// si assume sintassi -else->
	private String getAlternativeDirPageNr(String condition) {
		int virgola = condition.indexOf(',');
		String pageNr;
		if (virgola > 0) {
			pageNr = condition.substring(7, virgola);
		} else {
			pageNr = condition.substring(7);
		}
		return pageNr;
	}

	// si assume sintassi -else->
	private String getAlternativeDirDescription(String condition) {
		int virgola = condition.indexOf(',');
		String pageDesc;
		if (virgola > 0) {
			pageDesc = condition.substring(virgola + 1);
		} else {
			pageDesc = condition.substring(7);
		}
		return pageDesc;
	}

	private void conditionalLink(Page page, String condition, String alternative) {
		int endCondition = condition.indexOf(']');
		String evalCondition = condition.substring(1, endCondition);
		Map<String, Object> arguments = new HashMap<String, Object>();
		arguments.put("p", getP());
		arguments.put("np", getNp());
		Object result = null;
		try {
			result = engine.eval(evalCondition, arguments);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result instanceof Boolean) {
			if (((Boolean) result)) {
				page.addDirectionPageNr(getConditionDirPageNr(condition));
				page.addDirectionDescription(getConditionDirDescription(condition));
			} else {
				if (alternative != null) {
					page.addDirectionPageNr(getAlternativeDirPageNr(alternative));
					page.addDirectionDescription(getAlternativeDirDescription(alternative));
				}
			}
		}
	}

	private List<String> provaFight(Page page, List<String> arrayObj,
			String condition, String alternative) {

		Battle battle = getBattle(page);
		if (battle == null) {
			arrayObj.add("no-battle");
			return arrayObj;
		}
		battle.setPlayer(getP());
		battle.setNotPlayer(getNp());
		String esito = battle.round();
		// if loose
		if (esito.startsWith(Battle.FIGHT_PREFIX_LOOSE)) {
			// go to alternative page -else->
			String dirPageNr = getAlternativeDirPageNr(alternative);
			String dirDesc = getAlternativeDirDescription(alternative);
			arrayObj.add(dirPageNr);
			arrayObj.add(dirDesc);
			return arrayObj;
		}
		// if win
		else if (esito.startsWith(Battle.FIGHT_PREFIX_WIN)) {
			// go to win page --->
			String dirPageNr = getConditionDirPageNr(condition);
			String dirDesc = getConditionDirDescription(condition);
			arrayObj.add(dirPageNr);
			arrayObj.add(dirDesc);
			return arrayObj;
		}
		// else continue fighting
		else {
			arrayObj.add("wheel");
			arrayObj.add(esito.substring(0, 3));// prefisso numerico
												// format 000 con il
												// valore del dado.
			arrayObj.add(esito.substring(3));// descrizione testuale
												// dell'esito del dado.
			arrayObj.add(page.getKey());// dirlink
			arrayObj.add("Continua a combattere");// dirdesc
			// TODO eventuale link di fuga.
			return arrayObj;
		}
	}

	/**
	 * @deprecated
	 */
	private List<String> provaSpeciali(Page page, List<String> arrayObj,
			String condition1) {
		List<String> conditions = page.getConditions();
		int conditionPos = 0;
		for (String condition : conditions) {

			conditionPos++;
			if (condition.startsWith("[S:")) {
				String[] tokens = condition.split("[|=|]"); // [S:sort=ladro]
				if (tokens.length == 3
						&& getP().hasSpecial(tokens[1], tokens[2])) {
					arrayObj.add(page.getDirectionsPages().get(conditionPos));// dirlink
					arrayObj.add(page.getDirectionsDescription().get(
							conditionPos));// dirdesc
					return arrayObj;

				} else if (tokens.length == 2 && getP().hasSpecial(tokens[1])) {
					arrayObj.add(page.getDirectionsPages().get(conditionPos));// dirlink
					arrayObj.add(page.getDirectionsDescription().get(
							conditionPos));// dirdesc
					return arrayObj;

				}

			}
		}
		return arrayObj;

	}

	private List<String> whell(Page page, List<String> arrayObj,
			String condition) {

		int fato = Dice.roll.get6();
		arrayObj.add("wheel");
		arrayObj.add(String.format("%03d", fato));// prefisso
													// numerico
		// format 000 con il
		// valore del dado.
		arrayObj.add("La ruota dice " + fato + ".");// descrizione testuale
													// dell'esito del
		// dado.
		arrayObj.add(getConditionDirPageNr(condition));// dirlink
		arrayObj.add(getConditionDirDescription(condition));// dirdesc
		getP().setFato(fato);
		return arrayObj;

	}

	/**
	 * @deprecated
	 */
	private List<String> provaAbilita(Page page, List<String> arrayObj,
			String condition, String alternative) {

		String substring = page.getConditions().get(0).substring(8, 11);// +00
		int bonusmalus = new Integer(substring);
		int fato = Dice.roll.get6() + Dice.roll.get6();
		if (getP().getAbilita() - bonusmalus > fato) {
			arrayObj.add("wheel");
			arrayObj.add(String.format("%03d", fato));// prefisso
														// numerico
			// format 000 con il
			// valore del dado.
			arrayObj.add("");// descrizione testuale dell'esito del
								// dado.
			arrayObj.add(getConditionDirPageNr(condition));// dirlink
			arrayObj.add("Prova di abilità superata con successo.");// dirdesc
			return arrayObj;

		} else {
			arrayObj.add("wheel");
			arrayObj.add(String.format("%03d", fato));// prefisso
														// numerico
			// format 000 con il
			// valore del dado.
			arrayObj.add("");// descrizione testuale dell'esito del
								// dado.
			arrayObj.add(getAlternativeDirPageNr(alternative));// dirlink
			arrayObj.add("Prova di abilità non superata.");// dirdesc
			return arrayObj;

		}
	}

	private static void playPage(Player p, BufferedReader in, Page page) {
		System.out.println("BOOK PAGE:" + page);
		if (page.actions != null) {
			for (String action : page.actions) {
				// bonus/malus base
				if (action.startsWith("{mana")) {
					p.mana += new Integer(action.substring(5, 8));
				}
				if (action.startsWith("{stamina")) {
					String substring = action.substring(8, 11);
					p.stamina += new Integer(substring);
				}
				if (action.startsWith("{abilita")) {
					String substring = action.substring(8, 11);
					p.abilita += new Integer(substring);
				}
			}
		}
		if (page.conditions != null) {
			boolean esitoPositivo = false;
			for (String condition : page.conditions) {

				// sfida creatura
				if (condition.startsWith("[C:")) {
					Battle b = new Battle();
					NotPlayer np = new NotPlayer();
					if (b.fight(p, np, in)) {
						esitoPositivo = true;
						page.addDirection(condition);
					}
				}
				// sfida base
				if (condition.startsWith("[abilita")) {
					if (p.testAbilita()) {
						esitoPositivo = true;
						page.addDirection(condition);
					}
				}

			}
			// alternativa
			if (!esitoPositivo && page.alternative != null)
				System.out.println(page.alternative);
		}
		if (page.directions != null && page.directions.size() > 0) {
			System.out.println("DIRECTIONS:");
			for (String direction : page.directions) {
				System.out.println(direction);
			}
		}
	}

	public Bookmark getBookmark() {
		Bookmark bm = new Bookmark();
		bm.p = p.getDuplicate();
		bm.currentPageKey = this.currentPageKey;
		return bm;
	}

	public int saveBookmark() {
		Bookmark bm = getBookmark();
		bookmarks.add(bm);
		return bookmarks.size();
	}

	public String restoreBookmark(int bookMarkId) {
		Bookmark bm = bookmarks.get(bookMarkId);
		return restoreBookmark(bm);
	}

	public String restoreBookmark(Bookmark bm) {
		this.p = bm.p;
		this.np = null;
		this.currentPageKey = bm.currentPageKey;
		return currentPageKey;

	}

	public void removeBookmark(int bookMarkId) {
		bookmarks.remove(bookMarkId);
	}

	public List<Bookmark> getBookmarks() {
		return bookmarks;
	}

}
