package org.ffdw.gamebook;

import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class BookImporter {

	public Book read(InputStream is) {
		Book book = new Book();

		// Z means: "The end of the input but for the final terminator, if any"
		Scanner scanner = new Scanner(is, "UTF8");
		// legge il titolo del libro
		String line = scanner.nextLine();
		book.setTitle(line);

		boolean bookHasMorePages = true;
		while (bookHasMorePages) {
			bookHasMorePages = readNextPage(book, scanner);
		}

		scanner.close();
		return book;
	}

	public Book read(String bookName) {
		InputStream is = BookImporter.class
				.getResourceAsStream("/book/specchio.txt");
		return read(is);
	}

	private boolean readNextPage(Book book, Scanner scanner) {
		String key = null;
		String text = "";
		String line;
		int lineeVuote = 0;
		boolean pageEnded = false;
		boolean bookEnded = false;
		Page page = new Page();
		while (!pageEnded && !bookEnded) {
			try {
				line = scanner.nextLine().trim();
				if ("".equals(line)) {
					lineeVuote++;
					if (lineeVuote == 3) {
						lineeVuote = 0;
						pageEnded = true;
					}
				} else {
					// se la chiave è ancora assente la prima riga fa da chiave.
					if (key == null) {
						// esegue il parse della prima riga di testo
						parseFirstLine(line, page);
						key = page.getKey();
					} else {
						if (line.startsWith("[")) {
							parseCondition(line, page);
						} else if (line.startsWith("-else->")) {
							parseAlternative(line, page);
						} else if (line.startsWith("{")) {
							parseMarker(line, page);
						} else if (line.startsWith("image")) {
							parseSprite(line, page);
						} else if (line.startsWith("\"")) {
							parseTalk(line, page);
						} else if (line.startsWith("--->")) {
							parseLink(line, page);
						} else if (line.startsWith("+-->")) {
							parseInclude(line, page);
						} else
							text += line + "\n";
					}
				}
			} catch (NoSuchElementException e) {
				bookEnded = true;
			}
		}
		page.setText(text);
		book.addPage(key, page);
		return !bookEnded;
	}

	private void parseTalk(String line, Page page) {
		// skip prefix: "
		page.addTalk(line.substring(1, line.length() - 1));

	}

	private void parseSprite(String line, Page page) {
		// skip prefix: image
		page.addSprite(line.substring(6));

	}

	// <target-page-key>[,<testo>]
	private void parseLink(String line, Page page) {
		page.addDirection(line);
		int virgola = line.indexOf(',');
		if (virgola > 0) {
			page.addDirectionPageNr(line.substring(4, virgola));
			page.addDirectionDescription(line.substring(virgola + 1));
		} else {
			page.addDirectionPageNr(line.substring(4));
			page.addDirectionDescription(line.substring(4));
		}
	}

	// +-->
	private void parseInclude(String line, Page page) {
		page.addIncludePage(line.substring(4));
	}

	// <marker>
	private void parseMarker(String line, Page page) {
		page.addAction(line);
	}

	// <target-page-key>[,<testo>]
	// richiede la presenza di almeno una condizione
	private void parseAlternative(String line, Page page) {
		page.setAlternative(line);
	}

	// '['<condition>']'---><target-page-key>[,<testo>]
	private void parseCondition(String line, Page page) {
		if (line.startsWith("[C:")) {
			// a creature is here.
			// parse creature features.
			String[] tokens = line.split(":|]");
			String name = "noname";
			int stamina = 0, abilita = 0, mana = 0, resistenza = 0;
			for (String feature : tokens) {
				if (feature.toLowerCase().startsWith("stamina=")) {
					stamina = new Integer(feature.substring(8));
				} else if (feature.toLowerCase().startsWith("mana=")) {
					mana = new Integer(feature.substring(5));
				} else if (feature.toLowerCase().startsWith("abilita=")) {
					abilita = new Integer(feature.substring(8));
				} else if (feature.toLowerCase().startsWith("resistenza=")) {
					resistenza = new Integer(feature.substring(11));
				}
			}
			name = tokens[1];
			NotPlayer np = new NotPlayer(name, stamina, abilita, mana);
			np.setResistenza(resistenza);
			page.np = np;

		}
		page.addCondition(line);
	}

	/**
	 * Esempi di prime righe: <<key> <<key>,<<img> <<key>,<<img>,<<snd>
	 * <<key>,<<img>,<<snd>,<<titolo>
	 * 
	 * @param line
	 * @param page
	 */
	private void parseFirstLine(String line, Page page) {
		String[] keyTokens = line.split(",");
		if (keyTokens.length == 1) {
			page.setKey(line);
		} else if (keyTokens.length == 2) {
			page.setKey(keyTokens[0]);
			page.setImage(keyTokens[1]);
		} else if (keyTokens.length == 3) {
			page.setKey(keyTokens[0]);
			page.setImage(keyTokens[1]);
			page.setSound(keyTokens[2]);
		} else if (keyTokens.length == 4) {
			page.setKey(keyTokens[0]);
			page.setImage(keyTokens[1]);
			page.setSound(keyTokens[2]);
			page.setTitle(keyTokens[3]);
		}
	}
}
