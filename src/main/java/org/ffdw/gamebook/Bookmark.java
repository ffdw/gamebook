package org.ffdw.gamebook;

public class Bookmark {
	Player p;
	String currentPageKey;

	public String getCurrentPageKey() {
		return currentPageKey;
	}

	public String getString() {
		String bookmarkTxt = new String();
		bookmarkTxt = p.getString();
		bookmarkTxt += "," + currentPageKey;
		return bookmarkTxt;
	}

	public Bookmark setString(String bookmarkTxt) {
		this.p = (new Player()).setString(bookmarkTxt);
		String[] values = bookmarkTxt.split(",");
		currentPageKey = values[13];
		return this;
	}

	private static int nthOccurrence(String str, char c, int n) {
		int pos = str.indexOf(c, 0);
		while (n-- > 0 && pos != -1)
			pos = str.indexOf(c, pos + 1);
		return pos;
	}

}
