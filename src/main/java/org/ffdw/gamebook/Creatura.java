package org.ffdw.gamebook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class Creatura {

	private static final char CHAR_SEPARATOR = ',';
	private static final String VAL_SEPARATOR = "" + CHAR_SEPARATOR;

	private String nome;

	private String gilda;

	List<Oggetto> inventario = new ArrayList<Oggetto>();
	Map<String, Object> specials;
	Map<String, Object> markers;

	// Max size of inventary.
	int dimInventario = 10;

	Oggetto inUso;

	int level;

	// reflects how healthy and physically fit
	int stamina; // 22-
	int abilita; // 7-12
	int mana; // 4-9

	int maxstamina;
	int maxmana;
	int maxabilita;

	public Creatura() {
		level = 1;
		nome = "noname";
		gilda = "";
	}

	public Creatura randomize() {
		this.stamina = 20 + Dice.roll.get6() + Dice.roll.get6();
		maxstamina = stamina;
		abilita = Dice.roll.get6() + Dice.roll.get6();
		if (abilita < 7)
			abilita = 7;
		maxabilita = 12;
		mana = 3 + Dice.roll.get6();
		return this;
	}

	public Creatura(int level) {
		this.level = level;
		stamina = 18 + Dice.roll.get6() + Dice.roll.get6() + 2 * level;
		maxstamina = stamina;
		abilita = Dice.roll.get6() + 2 * level;
		maxabilita = 16;
		mana = 3 + Dice.roll.get6();
	}

	public boolean isDead() {
		return (stamina <= 0);
	}

	public int getLevel() {
		return level;
	}

	public int getStamina() {
		return stamina;
	}

	public void addStamina(int diff) {
		if ((stamina + diff) < 0) {
			stamina = 0;
			return;
		}
		if ((stamina + diff) > maxstamina) {
			stamina = maxstamina;
			return;
		}
		this.stamina += diff;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public void addMana(int diff) {
		if ((mana + diff) < 0) {
			mana = 0;
			return;
		}
		if ((mana + diff) > maxstamina) {
			mana = maxmana;
			return;
		}
		this.mana += diff;
	}

	public void addAbilita(int diff) {
		if ((abilita + diff) < 0) {
			abilita = 0;
			return;
		}
		if ((abilita + diff) > maxabilita) {
			abilita = maxabilita;
			return;
		}
		this.abilita += diff;
	}

	public int getAbilita() {
		return abilita;
	}

	public void addOggetto(Oggetto o) {
		if (inventario.size() < dimInventario)
			inventario.add(o);
		else
			System.out.println("Hai raggiunto massima capienza della Sacca.");
	}

	public void remOggetto(Oggetto o) {
		inventario.remove(o);
	}

	public List<Oggetto> getInventario() {
		return inventario;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public Oggetto getInUso() {
		return inUso;
	}

	public void setInUso(Oggetto inUso) {
		this.inUso = inUso;
	}

	public boolean testAbilita() {
		int sfida = Dice.roll.get6() + Dice.roll.get6();
		return (getAbilita() >= sfida);
	}

	public void att(String key, Object value) {
		putSpecial(key, value);
	}

	public void putSpecial(String key, Object value) {
		if (specials == null)
			specials = new TreeMap<String, Object>();
		specials.put(key, value);
	}

	public Object att(String key) {
		return getSpecial(key);
	}

	public Object inc(String key) {
		Object obj = getSpecial(key);
		if (obj != null && obj instanceof Integer) {
			obj = ((Integer) obj) + 1;
		}
		if (obj != null && obj instanceof Double) {
			obj = ((Double) obj) + 1;
		}
		return obj;
	}

	public Object getSpecial(String key) {
		if (specials == null)
			return null;
		return specials.get(key);
	}

	public boolean hasSpecial(String key) {
		if (specials == null)
			return false;
		return (specials.get(key) != null);
	}

	public boolean hasSpecial(String key, Object value) {
		if (specials == null)
			return false;
		return (specials.get(key).equals(value));
	}

	@Override
	public String toString() {
		return "Creatura [nome=" + nome + ", gilda=" + gilda + ", inventario="
				+ inventario + ", dimInventario=" + dimInventario + ", inUso="
				+ inUso + ", level=" + level + ", stamina=" + stamina
				+ ", abilita=" + abilita + ", mana=" + mana + ", maxstamina="
				+ maxstamina + ", maxmana=" + maxmana + ", maxabilita="
				+ maxabilita + "]";
	}

	public String getString() {
		String playerTxt = new String();
		playerTxt += this.nome + VAL_SEPARATOR;
		playerTxt += this.gilda + VAL_SEPARATOR;
		playerTxt += this.abilita + VAL_SEPARATOR;
		playerTxt += this.stamina + VAL_SEPARATOR;
		playerTxt += this.mana + VAL_SEPARATOR;
		playerTxt += this.maxabilita + VAL_SEPARATOR;
		playerTxt += this.maxmana + VAL_SEPARATOR;
		playerTxt += this.maxstamina + VAL_SEPARATOR;
		playerTxt += (inUso == null) ? "" : this.inUso.getString();
		return playerTxt;
	}

	public Creatura setString(String creaturaTxt) {
		String[] values = creaturaTxt.split("" + VAL_SEPARATOR);
		this.nome = values[0];
		this.gilda = values[1];
		this.abilita = new Integer(values[2]);
		this.stamina = new Integer(values[3]);
		this.mana = new Integer(values[4]);
		this.maxabilita = new Integer(values[5]);
		this.maxmana = new Integer(values[6]);
		this.maxstamina = new Integer(values[7]);
		return this;

	}

	public int getMaxstamina() {
		return maxstamina;
	}

	public void setMaxstamina(int maxstamina) {
		this.maxstamina = maxstamina;
	}

	public int getMaxmana() {
		return maxmana;
	}

	public void setMaxmana(int maxmana) {
		this.maxmana = maxmana;
	}

	public int getMaxabilita() {
		return maxabilita;
	}

	public void setMaxabilita(int maxabilita) {
		this.maxabilita = maxabilita;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;
	}

	public void setAbilita(int abilita) {
		this.abilita = abilita;
	}

	public String getGilda() {
		return gilda;
	}

	public void setGilda(String gilda) {
		this.gilda = gilda;
	}
}
