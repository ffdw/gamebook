package org.ffdw.gamebook;

public class Oggetto {

	private static final char CHAR_SEPARATOR = ',';
	private static final String VAL_SEPARATOR = "" + CHAR_SEPARATOR;
	String name;
	private int manaUso;
	// int manaConsumo;
	// int manaPossesso;
	private int abilita;
	private int stamina;
	private int attacco;
	private int difesa;
	// private int lancio;
	private String descrizione;

	int cardinalita = 1;
	// int valore;
	int disponibilita = 1;
	int usabilitaMin = 1;

	Oggetto() {

	}

	public Oggetto(String name, int dispo, int mana, int abilita, int stamina) {
		super();
		this.disponibilita = dispo;
		this.name = name;
		this.manaUso = mana;
		this.abilita = abilita;
		this.stamina = stamina;
	}

	public String getName() {
		return name;
	}

	public int getDisponibilita() {
		return disponibilita;
	}

	public void setAttacco(int attacco) {
		this.attacco = attacco;
	}

	public int getAttacco() {
		return attacco;
	}

	public int consumeAttacco() {
		if (disponibilita < usabilitaMin || attacco == 0) {
			return 0;
		}
		disponibilita--;
		return attacco;
	}

	public void setDifesa(int difesa) {
		this.difesa = difesa;
	}

	public int getDifesa() {
		return difesa;
	}

	public int consumeDifesa() {
		if (disponibilita < usabilitaMin || difesa == 0) {
			return 0;
		}
		disponibilita--;
		return difesa;
	}

	public void consume() {
		this.disponibilita--;
	}

	public int getAbilita() {
		return abilita;
	}

	public int getManaUso() {
		return manaUso;
	}

	public int getStamina() {
		return stamina;
	}

	@Override
	public String toString() {
		String testo = "(" + cardinalita + ") " + name;
		testo += "[manaUso=" + manaUso + ", abilita=" + abilita + ", stamina="
				+ stamina + ", attacco=" + attacco + ", difesa=" + difesa
				+ ", disponibilita=" + disponibilita + ", usabilitaMin="
				+ usabilitaMin + "]";
		if (descrizione != null)
			testo += " " + descrizione;
		return testo;
	}

	public Oggetto getDuplicate() {
		Oggetto o = new Oggetto();
		o.name = this.name;
		o.descrizione = this.descrizione;
		o.manaUso = this.manaUso;
		o.abilita = this.abilita;
		o.stamina = this.stamina;
		o.attacco = this.attacco;
		o.difesa = this.difesa;
		o.disponibilita = this.disponibilita;
		o.usabilitaMin = this.usabilitaMin;
		return o;
	}

	public String getString() {
		String oggettoTxt = new String();
		oggettoTxt += this.name + VAL_SEPARATOR;
		oggettoTxt += this.descrizione + VAL_SEPARATOR;
		oggettoTxt += this.manaUso + VAL_SEPARATOR;
		oggettoTxt += this.abilita + VAL_SEPARATOR;
		oggettoTxt += this.stamina + VAL_SEPARATOR;
		oggettoTxt += this.attacco + VAL_SEPARATOR;
		oggettoTxt += this.difesa + VAL_SEPARATOR;
		oggettoTxt += this.disponibilita + VAL_SEPARATOR;
		oggettoTxt += this.usabilitaMin;
		return oggettoTxt;
	}

	public String setString(String oggettoTxt) {
		String[] values = oggettoTxt.split("" + VAL_SEPARATOR);
		this.name = (values[0] == null) ? null : values[0];
		this.descrizione = (values[1] == null) ? null : values[1];
		this.manaUso = new Integer(values[2]);
		this.abilita = new Integer(values[3]);
		this.stamina = new Integer(values[4]);
		this.attacco = new Integer(values[5]);
		this.difesa = new Integer(values[6]);
		this.disponibilita = new Integer(values[7]);
		this.usabilitaMin = new Integer(values[8]);
		return oggettoTxt.substring(Oggetto.nthOccurrence(oggettoTxt,
				CHAR_SEPARATOR, 8));
	}

	private static int nthOccurrence(String str, char c, int n) {
		int pos = str.indexOf(c, 0);
		while (n-- > 0 && pos != -1)
			pos = str.indexOf(c, pos + 1);
		return pos;
	}
}
