package org.ffdw.gamebook;

public class NotPlayer extends Creatura {

	int resistenza;

	public NotPlayer() {
		super();
	}

	public NotPlayer(int level) {
		super(level);
		this.level = level;
	}

	public NotPlayer(String name, int stamina, int abilita, int mana) {
		super();
		this.setNome(name);
		this.stamina = stamina;
		this.mana = mana;
		this.abilita = abilita;
	}

	public NotPlayer duplicate() {
		NotPlayer np = new NotPlayer();
		np.stamina = this.stamina;
		np.mana = this.mana;
		np.abilita = this.abilita;
		return np;
	}

	public void setResistenza(int resistenza) {
		this.resistenza = resistenza;
	}

	public int getResistenza() {
		return resistenza;
	}
}
