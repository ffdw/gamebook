package org.ffdw.gamebook;

import java.util.Date;
import java.util.List;

public class GameBookInfo {

	protected String author;
	protected String title;
	protected String summary;
	protected String collection;
	protected Date publishedOn;
	protected String genere;
	protected int nrPages;
	protected List<BookConstrain> constrains;

	public GameBookInfo() {
	}

	public GameBookInfo(String author, String title, String summary,
			String collection, Date publishedOn, String genere, int nrPages,
			List<BookConstrain> constrains) {
		super();
		this.author = author;
		this.title = title;
		this.summary = summary;
		this.collection = collection;
		this.publishedOn = publishedOn;
		this.genere = genere;
		this.nrPages = nrPages;
		this.constrains = constrains;
	}

	public GameBookInfo(Book book) {
		this.title = book.getTitle();
		this.summary = book.getPage("INTRO").getText();
		this.nrPages = book.getNrPages();
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public Date getPublishedOn() {
		return publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public List<BookConstrain> getConstrains() {
		return constrains;
	}

	public void setConstrains(List<BookConstrain> constrains) {
		this.constrains = constrains;
	}

	public void setNrPages(int nrPages) {
		this.nrPages = nrPages;
	}

	public int getNrPages() {
		return nrPages;
	}

}
