package org.ffdw.gamebook;

public class Arena {

	NotPlayer np;
	Player p;

	public void fight() {
		Battle b = new Battle();
		np = new NotPlayer(3);
		p = new Player().randomize();
		Oggetto o = new Oggetto("spada", 1000, 5, 5, 0);
		o.setAttacco(5);
		o.setDifesa(2);
		p.addOggetto(o);
		o = new Oggetto("pozione della vita", 1, 0, 0, 5);
		p.addOggetto(o);
		o = new Oggetto("laser", 1, 50, 50, 50);
		o.setAttacco(50);
		o.setDifesa(50);
		p.addOggetto(o);
		b.fight(p, np);
	}

	public static void main(String[] args) {
		System.out.println("**** ARENA ****");
		new Arena().fight();

	}
}
