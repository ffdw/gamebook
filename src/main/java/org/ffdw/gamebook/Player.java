package org.ffdw.gamebook;

import java.util.ArrayList;

public class Player extends Creatura {

	private static final char CHAR_SEPARATOR = ',';
	private static final String VAL_SEPARATOR = "" + CHAR_SEPARATOR;

	int experience;
	// reflects your expertise in combat, your dexterity and agility
	int luck;
	// the result of last dice toss.
	int fato;

	public Player() {
		super();
	}

	public Player randomize() {
		super.randomize();
		luck = 6 + Dice.roll.get6();
		return this;
	}

	public void nextLevel() {
		experience++;
		abilita = abilita + 2 * getLevel();
		stamina = stamina + 2 * getLevel();
	}

	@Override
	public int getLevel() {
		if (experience < 8)
			return 1;
		if (experience < 16)
			return 2;
		if (experience < 25)
			return 3;
		if (experience < 36)
			return 4;
		return 5;
	}

	@Override
	public String toString() {
		return "Player [experience=" + experience + ", luck=" + luck
				+ ", inventario="
				+ inventario
				// + ", speciali=" + speciali + ", magie=" + magie
				+ ", inUso=" + inUso + ", level=" + level + ", stamina="
				+ stamina + ", abilita=" + abilita + "] POINTS [" + getPoints()
				+ "]";
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int value) {
		this.experience = value;
	}

	public int getPoints() {
		return experience + stamina + mana + abilita;
	}

	public Player getDuplicate() {
		Player p = new Player();
		p.abilita = this.abilita;
		p.stamina = this.stamina;
		p.mana = this.mana;
		p.experience = this.experience;
		p.maxabilita = this.maxabilita;
		p.maxmana = this.maxmana;
		p.maxstamina = this.maxstamina;
		p.luck = this.luck;
		if (p.inUso != null) {
			p.inUso = this.inUso.getDuplicate();
		}
		p.inventario = new ArrayList<Oggetto>();
		for (Oggetto oggettoPosseduto : inventario) {
			p.inventario.add(oggettoPosseduto.getDuplicate());
		}
		return p;
	}

	public String getString() {
		String playerTxt = new String();
		playerTxt += super.getString() + VAL_SEPARATOR;
		playerTxt += this.experience + VAL_SEPARATOR;
		playerTxt += this.luck + VAL_SEPARATOR;
		playerTxt += this.fato + VAL_SEPARATOR;
		playerTxt += (inUso == null) ? "" : this.inUso.getString();
		return playerTxt;
	}

	public Player setString(String playerTxt) {
		String[] values = playerTxt.split("" + VAL_SEPARATOR);
		super.setString(playerTxt);

		return this;
	}

	private static int nthOccurrence(String str, char c, int n) {
		int pos = str.indexOf(c, 0);
		while (n-- > 0 && pos != -1)
			pos = str.indexOf(c, pos + 1);
		return pos;
	}

	public void addExperience(int diff) {
		if ((experience + diff) < 0) {
			experience = 0;
			return;
		}
		this.experience += diff;
	}

	public int getFato() {
		return fato;
	}

	public void setFato(int fato) {
		this.fato = fato;
	}

	public int getDado6() {
		return Dice.roll.get6();
	}
}
