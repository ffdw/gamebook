package org.ffdw.gamebook;

import java.util.ArrayList;
import java.util.List;

public class Page {

	String key;
	String text;
	String image;
	String sound;
	String title;
	String script;

	List<String> talks;
	List<String> sprites;
	List<String> directions;
	List<String> includePages;
	List<String> directionsPages;
	List<String> directionsActions;
	List<String> directionsDescription;
	List<String> conditions;
	List<String> actions;
	List<String> creatures;
	List<String> oggetti;
	String alternative;

	NotPlayer np;// TODO To produce a duplicate() in gamebook registoIncontri

	// Battle battle;// TODO To remove it has to be in gamebook

	public Page() {
		image = "001.jpg";
	}

	public Page(String text) {
		this.text = text;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	public void addAction(String line) {
		if (actions == null)
			actions = new ArrayList<String>();
		actions.add(line);

	}

	public List<String> getActions() {
		return actions;
	}

	public void addCondition(String line) {
		if (conditions == null)
			conditions = new ArrayList<String>();
		conditions.add(line);
	}

	public void addDirection(String line) {
		if (directions == null)
			directions = new ArrayList<String>();
		directions.add(line);
	}

	public List<String> getConditions() {
		return conditions;
	}

	public void addDirectionPageNr(String line) {
		if (directionsPages == null)
			directionsPages = new ArrayList<String>();
		directionsPages.add(line);
	}

	public void addIncludePage(String line) {
		if (includePages == null)
			includePages = new ArrayList<String>();
		includePages.add(line);
	}

	public void addDirectionAction(String line) {
		if (directionsActions == null)
			directionsActions = new ArrayList<String>();
		directionsActions.add(line);
	}

	public void addDirectionDescription(String line) {
		if (directionsDescription == null)
			directionsDescription = new ArrayList<String>();
		directionsDescription.add(line);
	}

	public int getDirectionsNr() {
		if (directions == null)
			return 0;
		return directions.size();
	}

	public List<String> getDirectionsDescription() {
		return directionsDescription;
	}

	public List<String> getDirectionsPages() {
		return directionsPages;
	}

	public List<String> getDirectionsActions() {
		return directionsActions;
	}

	public List<String> getIncludePages() {
		return includePages;
	}

	public void setAlternative(String alternative) {
		this.alternative = alternative;
	}

	public String getAlternative() {
		return alternative;
	}

	public List<String> getDirections() {
		return directions;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public String getTextCleaned() {
		String texto = text;
		texto = texto.replace('"', ' ');
		texto = texto.replace('\n', ' ');
		return texto;
	}

	public NotPlayer getNp() {
		return np;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public void addSprite(String sprite) {
		if (sprites == null)
			sprites = new ArrayList<String>();
		sprites.add(sprite);
	}

	public List<String> getSprites() {
		return sprites;
	}

	public void addTalk(String talk) {
		if (talks == null)
			talks = new ArrayList<String>();
		talks.add(talk);
	}

	public List<String> getTalks() {
		return talks;
	}
}
